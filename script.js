//Get Users
const getData = () => {
	fetch("https://jsonplaceholder.typicode.com/users")
		.then((response)=>{
		return response.json();
	}).then((data)=>{
		let posts = "";
		data.forEach((post)=>{
			posts += `
				<div class="col-4 border border-secondary">
					<h2>Name: ${post.name}</h2>
					<p>Email: ${post.email}</p>
					<p>Company: ${post.company.name}</p>
					<p>Phone: ${post.phone}</p>
				</div>`
		})
		document.querySelector("#output").innerHTML = posts;
	})
	.catch((err)=>{
		console.log("rejected");
	})
}

//Add Users
const addPost = (e) =>{
	e.preventDefault();
	const name = document.querySelector("#name").value;
	const username = document.querySelector("#username").value;
	const email = document.querySelector("#email").value;
	const street = document.querySelector("#street").value;
	const suite = document.querySelector("#suite").value;
	const city = document.querySelector("#city").value;
	const zipcode = document.querySelector("#zipcode").value;
	const geoLat = document.querySelector("#geoLat").value;
	const geoLong = document.querySelector("#geoLong").value;
	const phone = document.querySelector("#phone").value;
	const website = document.querySelector("#website").value;
	const companyName = document.querySelector("#companyName").value;
	const companyCatchPhrase = document.querySelector("#companyCatchPhrase").value;
	const companyBS = document.querySelector("#companyBS").value;

	fetch("https://jsonplaceholder.typicode.com/users",{
		method: "POST",
		headers: {
			"Accept": "application/json, text/plain",
			"Content-type": "application/json"
		},
		body:JSON.stringify({
			name: name,
			username: username,
			email: email,
			address: {
				street: street,
				suite: suite,
				city: city,
				zipcode: zipcode,
				geo: {
					lat: geoLat,
					lng: geoLong
				}
			},
			phone: phone,
			website: website,
			company: {
				name: companyName,
				catchPhrase: companyCatchPhrase,
				bs: companyBS
			}
		})
	})
	.then(response=>response.json())
	.then(getData=>console.log(getData))
}


const btn1 = document.querySelector("#btn1");
btn1.addEventListener("click",getData);

const addForm = document.querySelector("#addPost");
addForm.addEventListener("submit",addPost);